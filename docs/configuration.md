# Configuration

configure the application runtime

| Name                              | Purpose                                                          | Defaults |
|-----------------------------------|------------------------------------------------------------------|----------|
| `APP_DISCORD_CLIENT_TOKEN`        | the token to authenticate against the Discord API with           | `''`     |
| `APP_RESPONSES_YAML_PATH`         | the location of the responses yaml file                          | `''`     |
| `APP_RATE_LIMIT_COOLDOWN_SECONDS` | how long until the same trigger phrase can be called, per member | `8`      |
| `APP_HTTP_PORT`                   | a port address for serving http on, for health and readiness     | `:8123`  |

## Responses definition

example of a responses config:

```yaml
---
^ping$: 
  - pong
^pong$:
  - ping
hello|good morning|good evening:
  - greetings
  - hey!
```

example back-and-forths:

> **User**: ping
> **Bot**: pong

> **User**: pong
> **Bot**: ping

> **User**: hello there!
> **Bot**: greetings

> **User**: good morning
> **Bot**: hey!
