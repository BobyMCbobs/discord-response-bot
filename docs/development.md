# Development

## create a Discord application for local testing

check out the [Discord applications](./deployment.md#discord-applications) section of the deployment docs.

## running locally

development is as simple as

```bash
go run ./...
```

## debugging

debug with delve

```bash
go install github.com/go-delve/delve/cmd/dlv@latest
dlv debug cmd/discord-response-bot/main.go
```

## extending

extra handlers can be added, like so

```go
package main

import (
	"log"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/BobyMCbobs/discord-response-bot/pkg/responsebot"
)

func main() {
	responsebot.NewResponder().AddHandlers(func(s *discordgo.Session, m *discordgo.MessageCreate) {
        // TODO your thing here
		log.Printf("message: %v\n", m)
	}).Listen()
}
```
