# Security

## Verify the container image with `cosign`

run the following command to verify a production image

```bash
cosign verify \
    --certificate-identity-regexp 'https://gitlab.com/BobyMCbobs/discord-response-bot//.gitlab-ci.yml@(refs/heads/main|refs/tags/.*)' \
    --certificate-oidc-issuer-regexp 'https://gitlab.com' \
    registry.gitlab.com/bobymcbobs/discord-response-bot:latest \
    -o text
```
