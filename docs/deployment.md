# Deployment

## Discord applications

1. head to [https://discord.com/developers/applications/](https://discord.com/developers/applications/)
2. create a new application, with a chosen name
3. add an app icon, name, description and tags as needed
4. navigate to settings -> oauth -> general and copy the client id
5. select settings -> bot on the left navigation
6. under authorization flow, turn public bot to off
7. under build-a-bot, select _reset token_, copying the token and saving it somewhere
8. under privileged gateway intents, enable all
9. click to save all changes
10. scroll down to bot permissions and input: send messages, and read message history; copying the permissions integer
11. navigate to the a page like `https://discord.com/oauth2/authorize?client_id=CLIENT_ID&scope=bot&permissions=PERMISSIONS_INTEGER`, replacing CLIENT_ID and PERMISSIONS_INTEGER with the values from earlier steps (4, 9)
12. accept the bot to join the server
13. write the token from step 6 to a .env file, env value or as a secret

above docs last verified 26/12/23

<!-- TODO: refine permissions instructions -->

## Run in Podman/Docker

run the container image on your local machine

```bash
podman run \
  -it --rm \
  -v "$PWD"/responses.yaml:/var/discord-response-bot/responses.yaml:z,ro \
  -e APP_RESPONSES_YAML_PATH=/var/discord-response-bot/responses.yaml \
  --env-file .env \
  registry.gitlab.com/bobymcbobs/discord-response-bot:latest
```

## Deployment in Kubernetes

create a namespace

```bash
kubectl create ns discord-response-bot
```

create a secret with the Discord client token

```bash
kubectl -n discord-response-bot create secret generic discord-response-bot \
  --from-literal=APP_DISCORD_CLIENT_TOKEN=MY_TOKEN_HERE
```

using the configs in the [config folder](./config/) install the app

```bash
kubectl -n discord-response-bot apply -k ./config/
```

## Production

### Health/metrics port

a health http server runs by default on `:8123`.

| Endpoint     | Description                         |
|--------------|-------------------------------------|
| `/_health`   | check for readiness                 |
| `/_shutdown` | shutdown the Discord client session |
| `/_metrics`  | get prometheus metrics              |
