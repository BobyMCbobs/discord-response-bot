package main

import (
	"log"

	"gitlab.com/BobyMCbobs/discord-response-bot/pkg/build"
	"gitlab.com/BobyMCbobs/discord-response-bot/pkg/responsebot"
)

func main() {
	log.Printf("launching Discord Response Bot (%v, %v, %v, %v)", build.AppBuildVersion, build.AppBuildHash, build.AppBuildDate, build.AppBuildMode)
	responsebot.NewResponder().Listen()
}
