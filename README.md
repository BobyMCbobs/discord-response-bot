# Discord response bot

> respond to a message with a random response if the message contains a trigger phrase

## Features

- respond with random phrase
- regex based trigger phrases
- basic rate limiter
- extensible code composition
- minimal application

## Response definitions

example of a responses config:

```yaml
---
^ping$: 
  - pong
^pong$:
  - ping
the magic word:
  - you found the magic word!
hello|good morning|good evening:
  - greetings
  - hey!
```

example back-and-forths:

> User: ping
> Bot: pong

> User: pong
> Bot: ping

> User: hello there!
> Bot: greetings

> User: good morning
> Bot: hey!

## Docs

read the docs over at [bobymcbobs.gitlab.io/discord-response-bot](https://bobymcbobs.gitlab.io/discord-response-bot) or in the [docs folder](./docs).

## License

Copyright 2019-2023 Caleb Woodbine.
This project is licensed under the [AGPL-3.0](http://www.gnu.org/licenses/agpl-3.0.html) and is [Free Software](https://www.gnu.org/philosophy/free-sw.en.html).
This program comes with absolutely no warranty.
