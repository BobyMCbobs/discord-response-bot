package responsebot

import (
	"context"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"sigs.k8s.io/yaml"
)

const (
	defaultCoolDownSecond = int64(8)
)

type responses []string

func (tr *responses) random() (response string) {
	var items []string = *tr
	switch len(items) {
	case 0:
		return ""
	case 1:
		return items[0]
	}
	if len(items) == 1 {
		return items[0]
	}
	return items[rand.Intn(len(items))]
}

type responsesConfig map[string]responses

func (re *responsesConfig) match(input string) (trigger string, responses responses) {
	for k, v := range *re {
		if regexp.MustCompile(k).Match([]byte(input)) {
			return k, v
		}
	}
	return "", nil
}

type responder struct {
	coolDownSeconds   int64
	rateLimit         map[string]map[string]time.Time
	responses         responsesConfig
	httpPort          string
	responsesFilePath string
	configReload      bool

	session    *discordgo.Session
	httpServer *http.Server
}

func (r *responder) loadResponsesFile(filePath string) error {
	if _, err := os.Stat(filePath); err != nil {
		return err
	}
	file, err := os.ReadFile(filePath)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(file, &r.responses); err != nil {
		return err
	}
	return err
}

func NewResponder() (res *responder) {
	if res == nil {
		res = &responder{}
	}
	token, ok := os.LookupEnv("APP_DISCORD_CLIENT_TOKEN")
	if !ok {
		log.Fatalf("error: missing token via APP_DISCORD_CLIENT_TOKEN")
		return nil
	}
	responsesFilePath, ok := os.LookupEnv("APP_RESPONSES_YAML_PATH")
	if !ok {
		log.Fatalf("error: missing responses file path via APP_RESPONSES_YAML_PATH")
		return nil
	}
	httpPort, ok := os.LookupEnv("APP_HTTP_PORT")
	if !ok {
		httpPort = ":8123"
	}
	if val, ok := os.LookupEnv("APP_CONFIG_RELOAD"); ok {
		res.configReload = val == "true"
	}
	res.httpPort = httpPort
	res.responsesFilePath = responsesFilePath
	coolDownSecondsString, ok := os.LookupEnv("APP_RATE_LIMIT_COOLDOWN_SECONDS")
	if !ok {
		res.coolDownSeconds = defaultCoolDownSecond
	} else {
		coolDownSeconds, err := strconv.Atoi(coolDownSecondsString)
		if err != nil {
			log.Fatalf("error: failed to parse cooldown seconds: %v", err)
			return nil
		}
		res.coolDownSeconds = int64(coolDownSeconds)
		if res.coolDownSeconds <= 0 {
			res.coolDownSeconds = defaultCoolDownSecond
		}
	}

	session, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Fatalf("error: failed to create Discord session: %v", err)
		return nil
	}
	session.AddHandler(res.HandlerRandomResponse)
	session.Identify.Intents = discordgo.IntentsGuildMessages | discordgo.IntentsMessageContent

	if err := res.loadResponsesFile(res.responsesFilePath); err != nil {
		log.Fatalf("error: failed to load responses file: %v", err)
		return nil
	}
	if res.responses == nil {
		log.Fatalf("error: found empty responses")
		return nil
	}
	mux := http.NewServeMux()
	mux.HandleFunc("/_health", func(w http.ResponseWriter, r *http.Request) {
		if res.session == nil || !res.session.DataReady {
			w.WriteHeader(http.StatusServiceUnavailable)
			if _, err := w.Write([]byte(`not ready`)); err != nil {
				log.Fatalf("error: failed to write http response: %v", err)
			}
			return
		}
		w.WriteHeader(http.StatusOK)
		if _, err := w.Write([]byte(`ready`)); err != nil {
			log.Fatalf("error: failed to write http response: %v", err)
		}
	})
	mux.HandleFunc("/_shutdown", func(w http.ResponseWriter, r *http.Request) {
		log.Println("shutting down Discord client session")
		if err := res.session.Close(); err != nil {
			log.Fatal(err)
		}
	})
	mux.Handle("/_metrics", promhttp.Handler())
	res.httpServer = &http.Server{
		Handler:      mux,
		Addr:         res.httpPort,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	res.session = session
	return res
}

func (r *responder) AddHandlers(handlers ...any) *responder {
	for _, v := range handlers {
		r.session.AddHandler(v)
	}
	return r
}

func (r *responder) SetCoolDownSecond(i int64) *responder {
	if i <= 0 {
		i = defaultCoolDownSecond
	}
	r.coolDownSeconds = i
	return r
}

func (r *responder) cleanupRateLimits() {
	for {
		if !r.session.DataReady {
			time.Sleep(1 * time.Second)
			continue
		}
		entriesRemoved := 0
		for tr, am := range r.rateLimit {
			for a, t := range am {
				if t.Before(time.Now()) {
					delete(r.rateLimit[tr], a)
					entriesRemoved++
				}
			}
		}
		if entriesRemoved > 0 {
			log.Printf("[rate limit] cleaned up %v entries", entriesRemoved)
		}
		time.Sleep(5 * time.Second)
	}
}

func (r *responder) configReloader() {
	if !r.configReload {
		return
	}
	for {
		time.Sleep(60 * time.Second)
		if err := r.loadResponsesFile(r.responsesFilePath); err != nil {
			log.Fatalf("error: failed to load responses file: %v", err)
		}
		log.Println("[config] reloaded")
	}
}

func (r *responder) Listen() {
	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	go r.configReloader()
	go r.cleanupRateLimits()
	go func() {
		log.Println("http server listening on", r.httpPort)
		if err := r.httpServer.ListenAndServe(); err != nil {
			log.Fatalf("error on listen and serve: %v", err)
		}
	}()
	if err := r.session.Open(); err != nil {
		log.Println("error: opening connection:", err)
		return
	}
	defer func() {
		if r.session.DataReady {
			return
		}
		r.session.Close()
	}()
	log.Println("Discord bot is live. Logged in as", r.session.State.User.ID)
	<-done
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := r.httpServer.Shutdown(ctx); err != nil {
		log.Fatalf("Server didn't exit gracefully %v", err)
	}
}

func (r *responder) HandlerRandomResponse(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID || m.Content == "" {
		return
	}
	trigger, re := r.responses.match(m.Content)
	if trigger == "" || re == nil {
		return
	}
	if r.rateLimit == nil {
		r.rateLimit = map[string]map[string]time.Time{}
	}
	if r.rateLimit[trigger] == nil {
		r.rateLimit[trigger] = make(map[string]time.Time)
	}
	if !r.rateLimit[trigger][m.Message.Author.ID].IsZero() && time.Now().Before(r.rateLimit[trigger][m.Message.Author.ID]) {
		log.Printf("[%v/%v] %v (%v) response for '%v' on cooldown (%v)\n", m.GuildID, m.ChannelID, m.Message.Author.Username, m.Message.Author.ID, trigger, time.Now().Unix()-r.rateLimit[trigger][m.Message.Author.ID].Unix())
		return
	}
	content := re.random()
	if content == "" {
		log.Printf("[notice] no responses set for trigger: %v\n", trigger)
		return
	}
	_, err := s.ChannelMessageSendReply(m.ChannelID, content, m.Reference())
	if err != nil {
		log.Printf("error: failed to write message response: %v\n", err)
		return
	}
	log.Printf("[%v/%v] %v (%v) triggered a response, with the word/pattern '%v'\n", m.GuildID, m.ChannelID, m.Message.Author.Username, m.Message.Author.ID, trigger)
	if val := r.rateLimit[trigger][m.Message.Author.ID]; val.IsZero() {
		r.rateLimit[trigger][m.Message.Author.ID] = time.Now().Add(time.Duration(r.coolDownSeconds) * time.Second)
	}
}
